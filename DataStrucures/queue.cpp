#include "queue.h"

/*
function that initialize queue q with maxLen size and len 0
input: pointer to the queue and size fot the queue maxLen
output: none
*/
void initQueue(queue* q, unsigned int size)
{
	q->maxLen = size;
	q->len = 0; //initial queue without any data
	q->data = new unsigned int[size];
}

/*
function that clean the queue
input: pointer to the queue
output: none
*/
void cleanQueue(queue* q)
{
	delete(q->data);
}

/*
function that check if the queue is full
input: pointer to the queue
output: is full
*/
bool isFull(queue* q)
{
	return q->len == q->maxLen;
}

/*
function that check if the queue is empty
input: pointer to the queue
output: is empty
*/
bool isEmpty(queue* q)
{
	return q->len == 0;
}

/*
function that adds new value to the queue
input: pointer to the queue and new value
output: none
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (!isFull(q))
	{
		q->data[q->len] = newValue;
		q->len += 1;
	}
	else
	{
		std::cout << "queue is full!" << std::endl;
	}
}

/*
function that remove from queue
input: pointer to queue
output: the removed int
*/
int dequeue(queue* q)
{
	int result = 0, i = 0;
	if (isEmpty(q))
	{
		result = -1;
	}
	else
	{
		result = q->data[0];
		for (i = 1; i < q->len; i++)
		{
			q->data[i - 1] = q->data[i];
		}
		q->len -= 1;
	}
	return result;
}