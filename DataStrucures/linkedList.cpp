#include "linkedList.h"

/*
function that add item to the list
input: pointer to the list and new data
output: none
*/
void addToList(node** head, unsigned int newData)
{
	node* temp = (*head);
	*head = new node;
	(*head)->data = newData;
	(*head)->next = temp;
}

/*
function that removes data from the head of the list
input: pointer to the list
output: none
*/
void removeFromList(node** head)
{
	node* temp = (*head)->next;
	delete(*head);
	*head = temp;
}

/*
function that deletes list
input: list
ouput: none
*/
void deleteList(node* head)
{
	if (head != nullptr)
	{
		deleteList(head->next);
		delete(head);
	}
}