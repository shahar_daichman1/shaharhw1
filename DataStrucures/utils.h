#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include "stack.h"

#define SIZE 10

void reverse(int* nums, unsigned int size);
int* reverse10();

#endif // UTILS_H