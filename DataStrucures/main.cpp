#include <iostream>
#include "queue.h"
#include "linkedList.h"
#include "stack.h"
#include "utils.h"

void printArr(int* arr, unsigned int size);

int main()
{
	const int size = 5;
	int arr[size] = { 1,2,3,4,5 };
	int* arr10 = nullptr;
	std::cout << "Original arr:\n";
	printArr(arr, size);
	reverse(arr, size);
	std::cout << "Reversed:\n";
	printArr(arr, size);
	std::cout << "Enter 10 numbers:\n";
	arr10 = reverse10();
	std::cout << "Reversed:\n";
	printArr(arr10, SIZE);
	delete(arr10);
}

/*
function that prints an array
input: arr and its size
output: none
*/
void printArr(int* arr, unsigned int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}