#include "stack.h"

/*
function that initialize values for the stack
input: pointer to stack
output: none
*/
void initStack(stack* s)
{
	s->data = new node*;
	*(s->data) = nullptr;
	s->len = 0;
}

/*
function that clears a stack
input: pointer to stack
output: none
*/
void cleanStack(stack* s)
{
	deleteList(*(s->data));
	delete(s->data);
	s->len = 0;
}

/*
function that pushes new element to the stack
input: pointer to stack and new element 
output: none
*/
void push(stack* s, unsigned int element)
{
	addToList(s->data, element);
	s->len += 1;
}

/*
function that pops an element from the stack
input: pointer to the stack
output: popped element
*/
int pop(stack* s)
{
	int result = 0;
	if (s->len == 0)
	{
		result = -1;
	}
	else
	{
		result = (*(s->data))->data;
		removeFromList(s->data);
		s->len -= 1;
	}
	return result;
}