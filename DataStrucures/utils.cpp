#include "utils.h"

/*
function that reverses an array
input: array and its size
output: none
*/
void reverse(int* nums, unsigned int size)
{
	stack* s = new stack;
	initStack(s);
	int i = 0;
	for (i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
	cleanStack(s);
	delete(s);
}

/*
function that scan 10 nummbers to an array from the user and reverse the array
input: none
output: the reversed array
*/
int* reverse10()
{
	int* result = new int[SIZE], i = 0;
	for (i = 0; i < SIZE; i++)
	{
		std::cin >> result[i];
	}
	reverse(result, SIZE);
	return result;
}