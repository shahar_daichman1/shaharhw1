#ifndef LINKEDLIST_H
#define LINKEDLIST_H

typedef struct node
{
	unsigned int data;
	struct node* next;
} node;

void addToList(node** head, unsigned int newData);
void removeFromList(node** head);
void deleteList(node* head);

#endif // !LINKEDLIST_H
